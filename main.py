import requests
import json
from jsonschema import validate
from jsonschema import Draft6Validator
import random
import time

url = "http://localhost:8080/api"
headers = {"Content-Type": "application/json; charset=utf-8"}
ids = []
def test_get_item_list_check_status_code_equals_200():
     response = requests.get(url + "/item")
     resp_body = response.json()
     for item in resp_body:
        ids.append(int(item["id"]))    
     assert response.status_code == 200

schema = {    
    "type" : "object",
    "properties" : {         
        "id": { "type": "number" },
        "name": { "type": "string" },
        "requireFridge": { "type": "boolean" },
        "capacity": { "enum": [10, 100] },             
        "packagingType": { "enum": ["Botella", "Caja"] }
    },
    "required": ["id", "name","requireFridge", "capacity", "packagingType"]
}

def find_element_in_list(element, list_element):
    try:
        index_element = list_element.index(element)
        return index_element
    except ValueError:
        return None

def test_get_item_validates_json_resonse_schema ():

    response = requests.get(url + "/item/2")
    
    # Validate response headers and body contents, e.g. status code.
    assert response.status_code == 200

    # Validate response content type header
    assert response.headers["Content-Type"] == "application/json"

    resp_body = response.json()

    # Validate will raise exception if given json is not
    # what is described in schema.
    validate(instance=resp_body, schema=schema)

def post_item_validates_json_resonse_schema (data):
        
    endpoint = url + "/item"
    response = requests.post(url = endpoint ,  headers=headers, json=data)
    
    # Validate response headers and body contents, e.g. status code.
    assert response.status_code == 200

    # Validate response content type header
    assert response.headers["Content-Type"] == "application/json"

    resp_body = response.json()

    print(resp_body, "\n")
    # Validate will raise exception if given json is not
    # what is described in schema.
    validate(instance=resp_body, schema=schema)

def put_item_validates_json_resonse_schema (data):
        
    endpoint = url + "/item" 
    response = requests.put(url = endpoint ,  headers=headers, json=data)
    
    # Validate response headers and body contents, e.g. status code.
    assert response.status_code == 200

    # Validate response content type header
    assert response.headers["Content-Type"] == "application/json"

    resp_body = response.json()

    # Validate will raise exception if given json is not
    # what is described in schema.
    validate(instance=resp_body, schema=schema)    

def delete_item_validates_json_resonse_schema (id):
        
    endpoint = url + "/item/" + str(id) 
    response = requests.delete(url = endpoint ,  headers=headers)
    
    # Validate response headers and body contents, e.g. status code.
    assert response.status_code == 200

    # Validate response content type header
    assert response.headers["Content-Type"] == "application/json"


def test_multiple_insert ():
    print("\nIndique el numero de recursos a crear:")
    input_n = input()
    for i in range(0,int(input_n)):
        data = {
            "name": "testing creando nombre " + str(i),
            "requireFridge": random.choice([True,False]),
            "capacity":  random.choice([10,100]),
            "packagingType": random.choice(["Botella","Caja"])
        }
        print("Creando Item con data:")
        post_item_validates_json_resonse_schema(data)
        time.sleep(2)

def test_multiple_update ():
    print("\nIndique el numero de recursos a actualizar:")
    input_n = input()
    selected = []
    for i in range(0,int(input_n)):
        id = random.choice(ids)
        data = {
            "id": id,
            "name": "testing actualizando nombre " + str(i),
            "requireFridge": random.choice([True,False]),
            "capacity":  random.choice([10,100]),
            "packagingType": random.choice(["Botella","Caja"])
        }
        print("Actualizando ID:", id, "\nData:", data)
        put_item_validates_json_resonse_schema(data)
        time.sleep(2)

def test_multiple_delete ():
    print("\nIndique el numero de recursos a eliminar:")
    input_n = input()
    selected = []
    for i in range(0,int(input_n)):
        id = random.choice(ids) 
        while find_element_in_list(id, selected) != None:
            id = random.choice(ids) 
        selected.append(id)                    
        print("Eliminando ID:", id)
        delete_item_validates_json_resonse_schema(id)  
        time.sleep(2)

